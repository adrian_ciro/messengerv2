#Kotlin Messenger with Firebase Version 2

Improved from [https://www.letsbuildthatapp.com/course/Kotlin-Firebase-Messenger](https://www.letsbuildthatapp.com/course/Kotlin-Firebase-Messenger)

Working with android PIE, android studio 3.4.

This is a better version of a chat working with fragments, safe args, view model architecture, nav graph with animations.

