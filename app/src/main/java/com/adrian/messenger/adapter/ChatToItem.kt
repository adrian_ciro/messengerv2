package com.adrian.messenger.adapter

import com.adrian.messenger.R
import com.adrian.messenger.model.User
import com.squareup.picasso.Picasso
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.row_chat_to.view.*


class ChatToItem(val text: String, val user: User): Item<ViewHolder>() {
  override fun bind(viewHolder: ViewHolder, position: Int) {
    viewHolder.itemView.textview_to_row.text = text

    val uri = user.profileImageUrl
    val targetImageView = viewHolder.itemView.imageview_chat_to_row

    if (uri.equals("")) {
      Picasso.get().load(R.drawable.user).into(targetImageView)
    } else {
      Picasso.get().load(uri).into(targetImageView)
    }
  }

  override fun getLayout(): Int {
    return R.layout.row_chat_to
  }
}

