package com.adrian.messenger.adapter

import com.adrian.messenger.R
import com.adrian.messenger.model.ChatMessage
import com.adrian.messenger.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.row_latest_message.view.*


class LatestMessageItem(val chatMessage: ChatMessage): Item<ViewHolder>() {

    var chatWithUser: User? = null

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.message_textview_latest_message.text = chatMessage.text

        val chatPartnerId: String
        if(chatMessage.fromId == FirebaseAuth.getInstance().uid){
            chatPartnerId= chatMessage.toId
        }
        else{
            chatPartnerId= chatMessage.fromId
        }

        val ref = FirebaseDatabase.getInstance().getReference("/users/$chatPartnerId")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(p0: DataSnapshot) {
                chatWithUser = p0.getValue(User::class.java)
                viewHolder.itemView.username_textview_latest_message.text = chatWithUser?.username

                val targetImageView = viewHolder.itemView.imageview_latest_message
                Picasso.get().load(chatWithUser?.profileImageUrl).into(targetImageView)
            }
        })


    }

    override fun getLayout(): Int {
        return R.layout.row_latest_message
    }
}