package com.adrian.messenger.adapter

import com.adrian.messenger.R
import com.adrian.messenger.model.User
import com.squareup.picasso.Picasso
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.row_user_new_message.view.*

class UserItem(val user: User): Item<ViewHolder>() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.username_textview_new_message.text = user.username

        if(user.profileImageUrl.isNotEmpty())
            Picasso.get().load(user.profileImageUrl).into(viewHolder.itemView.imageview_new_message)
        else
            Picasso.get().load(R.drawable.user).into(viewHolder.itemView.imageview_new_message)

    }

    override fun getLayout(): Int {
        return R.layout.row_user_new_message
    }
}