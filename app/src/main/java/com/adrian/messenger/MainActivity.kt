package com.adrian.messenger

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import com.adrian.messenger.databinding.ActivityMainBinding
import com.adrian.messenger.fragment.LatestMessagesFragment
import com.adrian.messenger.fragment.LatestMessagesFragmentDirections
import com.google.firebase.auth.FirebaseAuth


class MainActivity : AppCompatActivity() {

    private val TAG = "Main"
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setupNavigation()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.menu_new_message -> {
                Log.i(TAG, "Create new chat click event")
                findNavController(R.id.navHostFragment).navigate(
                    LatestMessagesFragmentDirections.actionLatestMessagesFragmentToNewChatFragment(
                        LatestMessagesFragment.currentUser!!
                    )
                )
            }
            R.id.menu_sign_out -> {
                FirebaseAuth.getInstance().signOut()
                Log.i(TAG, "Signout click event")
                findNavController(R.id.navHostFragment).navigate(LatestMessagesFragmentDirections.actionLatestMessagesFragmentToUserRegistrationFragment())

            }
            else -> {
                Log.i(TAG, "Click event on toolbar ${item?.itemId}")
                findNavController(R.id.navHostFragment).navigate(R.id.latestMessagesFragment)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun replaceFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.container, fragment)
        fragmentTransaction.commit()
    }

    private fun setupNavigation() {
        // first find the nav controller
        val navController = findNavController(R.id.navHostFragment)

        navController.addOnDestinationChangedListener { _, destination: NavDestination, _ ->
            val toolBar = supportActionBar ?: return@addOnDestinationChangedListener
            when (destination.id) {
                R.id.userRegistrationFragment -> {
                    toolBar.hide()
                }
                R.id.loginFragment -> {
                    toolBar.hide()
                }
                R.id.newChatFragment -> {
                    toolBar.title = getString(R.string.select_user)
                    toolBar.setDisplayHomeAsUpEnabled(true)
                }
                R.id.chatLogFragment -> {
                    toolBar.setDisplayHomeAsUpEnabled(true)
                }
                else -> {
                    toolBar.title = getString(R.string.app_name)
                    toolBar.setDisplayHomeAsUpEnabled(false)
                    toolBar.show()
                }
            }
        }
    }

}
