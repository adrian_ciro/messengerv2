package com.adrian.messenger.fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.adrian.messenger.R
import com.adrian.messenger.databinding.FragmentNewChatBinding
import com.adrian.messenger.model.User
import com.adrian.messenger.adapter.UserItem
import com.adrian.messenger.viewmodel.NewChatViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder

class NewChatFragment : Fragment() {

    companion object {
        var currentUser: User? = null
    }
    private val TAG = "New Chat"
    private lateinit var viewModel: NewChatViewModel
    private lateinit var binding: FragmentNewChatBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_chat, container, false)

        binding.lifecycleOwner = this

        setHasOptionsMenu(true)

        val safeArgs: NewChatFragmentArgs by navArgs()
        currentUser = safeArgs.user

        fetchUsers()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(NewChatViewModel::class.java)
        binding.viewModel = viewModel
    }

    private fun fetchUsers() {
        val ref = FirebaseDatabase.getInstance().getReference("/users")
        ref.addListenerForSingleValueEvent(object: ValueEventListener {

            override fun onDataChange(p0: DataSnapshot) {
                val adapter = GroupAdapter<ViewHolder>()

                p0.children.forEach {
                    Log.d("NewMessage", it.toString())
                    val user = it.getValue(User::class.java)
                    if (user != null && user.uid != currentUser?.uid) {
                        adapter.add(UserItem(user))
                    }
                }

                adapter.setOnItemClickListener { item, _ ->

                    val userItem = item as UserItem
                    findNavController().navigate(NewChatFragmentDirections.actionNewChatFragmentToChatLogFragment(
                        userItem.user))
                    Log.i(TAG, "Click on user ${userItem.user.username}")

                }

                binding.recyclerviewNewmessage.adapter = adapter
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

}
